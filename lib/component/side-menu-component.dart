import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/services/setting-services.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

class SideMenu extends StatefulWidget {
  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  @override
  Widget build(BuildContext context) {
    AppData appData = Provider.of<AppData>(context);

    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 52.0),
              decoration: BoxDecoration(
                  color: kPrimaryColor,
              ),
            child: Image.asset('images/LogoPNG.png', height: 130.0,),
          ),
          Container(
            decoration: BoxDecoration(
              color: kPrimaryColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black38,
                  blurRadius: 2.0,
                  spreadRadius: 0.0,
                  offset: Offset(-2.0, 2.0), // shadow direction: bottom right
                )
              ]
            ),
            height: 48.0,
              child:  Center(child: Text(appData.labels['store.tagline'],style: kTitleTextStyle,))
          ),
          Expanded(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    //Language
                    Text(appData.labels['sidemenu.lang.label'],style: kOrderIngredientsTextStyle,),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: LanguageSelector(),
                    ),
                    //Currency
                    Text(appData.labels['sidemenu.currency.label'],style: kOrderIngredientsTextStyle,),
                    ListTile(
                      leading: Text(appData.labels['settings.currency.cz'],style: kTotalTextStyle,),
                      title: Text(appData.labels['settings.currency.cz.name'],style: kOrderTitleTextStyle,),
                    ),
                    //Contact
                    Text(appData.labels['sidemenu.contact.label'],style: kOrderIngredientsTextStyle,),
                    ListTile(
                      leading: Icon(FontAwesomeIcons.phone),
                      title: Text(appData.labels['store.phonenumber'],style: kOrderTitleTextStyle,),
                    ),
                    ListTile(
                      leading: Icon(FontAwesomeIcons.mapPin),
                      title: Text(appData.labels['store.address'],style: kOrderTitleTextStyle,),
                    ),


                  ],
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            child: ListTile(
              leading: Icon(FontAwesomeIcons.codeBranch),
              title: Text('${appData.packageInfo.version}+${appData.packageInfo.buildNumber}',style: kOrderTitleTextStyle),
              subtitle: Text('Version',style: kOrderIngredientsTextStyle),
            ),
          )
        ],
      ),
    );
  }
}

class LanguageSelector extends StatefulWidget {
  @override
  _LanguageSelectorState createState() => _LanguageSelectorState();
}

class _LanguageSelectorState extends State<LanguageSelector> {
  @override
  Widget build(BuildContext context) {
    AppData appData = Provider.of<AppData>(context);

    return StreamBuilder<QuerySnapshot>(
      stream: SettingServices().getAvailableLanguage(),
      builder: (context, snapshot){

        if(!snapshot.hasData){
          return ListTile(
            title: CircularProgressIndicator(),
          );
        }

        List<DropdownMenuItem<String>> languages = [];

        var documents = snapshot.data.documents;

        documents.forEach((document) {
          var item = DropdownMenuItem<String>(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: <Widget>[
                  Image.network(document.data['icon'],width: 30.0,height: 30.0,),
                  SizedBox(width: 10.0,),
                  Text(document.data['lang']),
                ],
              ),
            ),
            value: document.documentID,
          );

          languages.add(item);
        });

        return DropdownButton<String>(
          isExpanded: true,
          value: appData.lang,
          items:languages,
          onChanged: (value){
            print(value);
            setState(() {
              appData.lang = value;
              appData.updateLabelsByLang(value);
              Navigator.pop(context);
            });
          },
        );

      },
    );
  }
}

