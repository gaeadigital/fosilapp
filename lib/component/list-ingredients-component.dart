import 'package:flutter/material.dart';
import 'package:fosilapp/component/card-ingredient-component.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/model/group-ingredient.dart';
import 'package:fosilapp/model/ingredient.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

class ListIngredients extends StatefulWidget {

  final List<GroupIngredient> groups;
  final Function updateContentIngredient;

  ListIngredients({this.groups, this.updateContentIngredient});

  @override
  _ListIngredientsState createState() => _ListIngredientsState();
}

class _ListIngredientsState extends State<ListIngredients> {

  buildGroupList(List<GroupIngredient> groups, AppData appData){
    List<Widget> groupsWidget= [];

    groups.forEach((group) {

      Widget wdgGroup = Padding(padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(appData.labels[group.label], style: kGroupIngredientTextStyle,),
              ),
              Column(
                children: buildIngredients(group.ingredients,appData),
              )
            ],
          ));
      groupsWidget.add(wdgGroup);
    });

    return groupsWidget;
  }

  buildIngredients(List<Ingredient> ingredients, AppData appData){
    List<Widget> ingredientsWidget= [];

    ingredients.forEach((ingredient) {
      Widget ingredientCard = CardIngredient(ingredient:ingredient ,updateContent: widget.updateContentIngredient,);
      ingredientsWidget.add(ingredientCard);
    });

    return ingredientsWidget;
  }

  @override
  Widget build(BuildContext context) {

    AppData appData = Provider.of<AppData>(context);

    return ListView(
      children: buildGroupList(widget.groups,appData),
    );
  }
}