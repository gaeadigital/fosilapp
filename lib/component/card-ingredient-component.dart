import 'package:flutter/material.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/model/ingredient.dart';
import 'package:provider/provider.dart';

class CardIngredient extends StatefulWidget {
  final Ingredient ingredient;
  final Function updateContent;

  CardIngredient({this.ingredient,this.updateContent});

  @override
  _CardIngredientState createState() => _CardIngredientState();
}

class _CardIngredientState extends State<CardIngredient> {
  @override
  Widget build(BuildContext context) {
    AppData appData = Provider.of<AppData>(context);

    return GestureDetector(
      onTap: (){
        setState(() {
          if(widget.ingredient.check){
            widget.ingredient.check = false;
            widget.updateContent(widget.ingredient);
          } else {
            widget.ingredient.check = true;
            widget.updateContent(widget.ingredient);
          }
        });
      },
      child: Card(
        child: ListTile(
          title: Text(appData.labels[widget.ingredient.label]),
          trailing: (widget.ingredient.check== true)?Icon(Icons.check_circle,color: Colors.green,):Text('${widget.ingredient.amount} ${appData.labels['settings.currency.cz']}'),
        ),
      ),
    );
  }
}
