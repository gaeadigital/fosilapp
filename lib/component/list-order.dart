import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/model/content.dart';
import 'package:fosilapp/model/ingredient.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

AppData appData;

class ListOrder extends StatefulWidget {
  
  final List<Content> orders;
  final Function updateAmount;
  
  ListOrder({this.orders, this.updateAmount});
  
  @override
  _ListOrderState createState() => _ListOrderState();
}

class _ListOrderState extends State<ListOrder> {


  getIngredientString(List<Ingredient> order){
    String allIngredients='';
    for (var ingredient in order) {
      allIngredients+=appData.labels[ingredient.label]+', ';
    }
    return allIngredients;
  }


  @override
  Widget build(BuildContext context) {
    appData = Provider.of<AppData>(context);

    return ListView.builder(
      itemCount: appData.order.order.length,
      itemBuilder: (context, index){
        var element = appData.order.order[index];
        return Padding(
          padding: EdgeInsets.only(top: 16.0),
          child: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(FontAwesomeIcons.trashAlt),
                onPressed: (){
                  setState(() {
                    appData.order.totalAmount -=appData.order.order[index].total;
                    appData.order.order.removeAt(index);
                    appData.removeBurrito();
                  });
                },
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text('Burrito',style: kOrderTitleTextStyle,),
                    Text(getIngredientString(element.contents), style: kOrderIngredientsTextStyle,)
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text('${element.total} ${appData.labels['settings.currency.cz']}' ,style: kOrderTitleTextStyle,),
              )
            ],
          ),
        );
      },
    );
  }
}
