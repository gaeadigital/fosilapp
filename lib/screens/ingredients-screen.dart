import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fosilapp/component/list-ingredients-component.dart';
import 'package:fosilapp/component/side-menu-component.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/model/content.dart';
import 'package:fosilapp/model/group-ingredient.dart';
import 'package:fosilapp/model/ingredient.dart';
import 'package:fosilapp/screens/order-screen.dart';
import 'package:fosilapp/services/ingrediente-service.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

AppData appData;

class IngredientsScreen extends StatefulWidget {
  @override
  _IngredientsScreenState createState() => _IngredientsScreenState();
}

class _IngredientsScreenState extends State<IngredientsScreen> {
  num amount=0.0;
  Content content = Content();

  List<GroupIngredient> ingredients;
  List<Ingredient> selectedIngredients;

  startAmount(num initialAmount){
   amount = initialAmount;
  }

  updateAmount(){
    double total = 0.0;
    content.contents.forEach((element) {
      total += element.amount;
    });
    setState(() {
      amount= total;
    });
  }

  startContent(){
    ingredients.forEach((group) {
      group.ingredients.forEach((ingredient) {
        if(ingredient.check){
          if(existIngredientOnContent(ingredient) == -1) {
            content.contents.add(ingredient);
          }
        }
      });
    });
    updateAmount();
  }

  updateContent(Ingredient ingredient){

    if(ingredient.check){
      if(existIngredientOnContent(ingredient) == -1) {
        content.contents.add(ingredient);
      }
    } else {
      content.contents.removeAt(existIngredientOnContent(ingredient));
    }
    updateAmount();
  }

  existIngredientOnContent(Ingredient ingredient){
    int existIndex = -1;
    for(var i= 0; i <content.contents.length;i++) {
      if (ingredient.id == content.contents[i].id) {
       existIndex = i;
      }
    }
    return existIndex;
  }

  addBurritoToOrder(){
    content.total = amount;
    appData.order.order.add(content);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appData = Provider.of<AppData>(context);
    ingredients = List.from(appData.groups);
    startContent();
    return Scaffold(
      drawer: SideMenu(),
      appBar: AppBar(
        title: Text(appData.labels['order.makemyburrito']),
        actions: <Widget>[
          (appData.order.order.length>0)?IconButton(
            icon: Icon(Icons.close),
            onPressed: (){
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) {
                    return OrderScreen();
                  },
                ),
              );
            },
          ):Column()
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            color: kSecondaryColor,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(child: Text('Total',style: kTotalAmountTextStyle,)),
                  Text('${amount} ${appData.labels['settings.currency.cz']}',style: kTotalAmountTextStyle,)
                ],),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Expanded(child: Text(appData.labels['menu.ingredients.select'],style: kInstructionsTextStyle,)),
                Icon(Icons.check_box,color: kAccentColor,),
              ],
            ),
          ),
          Expanded(
            child: ListIngredients(groups: ingredients,updateContentIngredient: updateContent,),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  ),
              padding: EdgeInsets.all(16.0),
              color: kAccentColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.add_shopping_cart,color: Colors.white,),
                  SizedBox(
                    width: 8.0,
                  ),
                  Text(appData.labels['order.additem'],style: kButtonTextStyle,),
                ],
              ),
              onPressed: (){
                addBurritoToOrder();
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) {
                      return OrderScreen();
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}




class GroupIngredientStream extends StatelessWidget {

  final Function startAmount;
  final Function updateAmount;

  final Function startContent;
  final Function updateContent;

  GroupIngredientStream({this.startAmount, this.updateAmount,this.startContent, this.updateContent});

  buildGroups(List<GroupIngredient> groups){
    List<ListTile> groupsTiles = [];
    num initialAmount = 0;
    groupsTiles.add(ListTile(
        leading: Icon(Icons.info_outline,color: kAccentColor,),
        title: Text(appData.labels['menu.ingredients.select'],style: kInstructionsTextStyle,)
    ));

 if(groups.isNotEmpty) {
   groups.forEach((group) {
     var tile = ListTile(
         title: Text(appData.labels[group.label],style: kGroupIngredientTextStyle,)
     );
     groupsTiles.add(tile);

     group.ingredients.forEach((ingredient) {
       if(ingredient.check){
         initialAmount+=ingredient.amount;
         startContent(ingredient);
       }
       var tile = ListTile(
           title: GestureDetector(
             onTap: (){
               if(ingredient.check){
                 ingredient.check = false;
               } else{
                 ingredient.check = true;
               }
               updateAmount(ingredient);
               updateContent(ingredient);
             },
             child: Card(
               child: ListTile(
                 title: Text(appData.labels[ingredient.label]),
                 trailing: (ingredient.check== true)?Icon(Icons.check_circle,color: Colors.green,):Text('${ingredient.amount} ${appData.labels['settings.currency.cz']}'),
               ),
             ),
           )
       );
       groupsTiles.add(tile);
     });

   });
   startAmount(initialAmount);
 }
    return groupsTiles;
  }

  @override
  Widget build(BuildContext context) {
    appData = Provider.of<AppData>(context);
    return ListView(
      children: buildGroups(appData.groups),
    );
    /*return StreamBuilder<QuerySnapshot>(
      stream: IngredientServices().get(),
      builder: (context, snapshot){
        if(!snapshot.hasData){
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        final groupsDocs = snapshot.data.documents;
        List<ListTile> groups = [];

        groups.add(ListTile(
          leading: Icon(Icons.info_outline,color: kAccentColor,),
          title: Text(appData.labels['menu.ingredients.select'],style: kInstructionsTextStyle,)
        ));

        for(var groupDoc in groupsDocs) {

          var tile = ListTile(
            title: IngredientsStream(groupId: groupDoc.documentID,
                groupLabel: groupDoc.data['label'])
          );

          groups.add(tile);
        }

        return ListView(
          children: groups,
        );
      },
    );*/
  }
}

class IngredientsStream extends StatelessWidget {

  final String groupId;
  final String groupLabel;

  IngredientsStream({this.groupId, this.groupLabel});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: IngredientServices().getByGroup(groupId),
      builder: (context, snapshot){
        if(!snapshot.hasData){
          return Center(
            child: LinearProgressIndicator(),
          );
        }

        final ingredientsDocs = snapshot.data.documents;
        List<Widget> ingredients = [];

        ingredients.add( Padding(
          padding: const EdgeInsets.only(top: 24.0, left: 16.0, bottom: 8.0),
          child: Text(appData.labels[groupLabel],style: kGroupIngredientTextStyle,),
        ));


        for(var ingredient in ingredientsDocs){
          // Validamos si el campo check existe y esta en true.
          // cargamos el valor del ingrediente dentro del monto total.
          ingredients.add(Card(
            child: ListTile(
              title: Text(appData.labels[ingredient.data['label']]),
              trailing: (ingredient.data['check']== true)?Icon(Icons.check_circle,color: Colors.green,):Text('${ingredient.data['amount']} ${appData.labels['settings.currency.cz']}'),
            ),
          ));
        }

        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: ingredients,
        );
      },
    );
  }
}
