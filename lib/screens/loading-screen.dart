import 'package:flutter/material.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/screens/new-order-screen.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

AppData appData;
String componentLoading = 'Please wait...';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();

}

class _LoadingScreenState extends State<LoadingScreen> {

  loading() async{
    appData.lang = 'cs-CZ';
    await appData.getLabelsByLang(appData.lang);
    await appData.getGroupIngredients();
    await appData.getDistancesAmount();
    await appData.getSettings();
    await appData.newEmptyOrder();
    await appData.getPackageInfo();

    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) {
          return NewOrderScreen();
        },
      ),
    );
  }

 @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    appData = Provider.of<AppData>(context);
    loading();
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Image.asset('images/musle.jpg'),
          Text(componentLoading,style: kButtonTextStyle,textAlign: TextAlign.center,)
        ],
      ),
    );
  }
}
