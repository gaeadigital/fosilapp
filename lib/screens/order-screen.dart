import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fosilapp/component/list-order.dart';
import 'package:fosilapp/component/side-menu-component.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/screens/confirm-checkout-screen.dart';
import 'package:fosilapp/screens/ingredients-screen.dart';
import 'package:fosilapp/screens/new-order-screen.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

class OrderScreen extends StatefulWidget {
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  AppData appData;

  updateTotalAmount() {
    double total = 0.0;
    appData.order.order.forEach((element) {
      total += element.total;
    });
    setState(() {
      appData.order.totalAmount = total;
    });
  }

  @override
  Widget build(BuildContext context) {
    appData = Provider.of<AppData>(context);
    updateTotalAmount();
    return Scaffold(
      drawer: SideMenu(),
      appBar: AppBar(
        title: Text(appData.labels['order.myorder']),
        actions: <Widget>[
          IconButton(
            icon: Icon(
        Icons.delete_forever,
        color: Colors.white,
      ),
            onPressed:(){
              showDialog(context: context,
                  builder: (BuildContext context){
                    return SimpleDialog(
                      title: Text(appData.labels['admin.order.detail.cancel'],style: kGroupIngredientTextStyle,),
                      contentPadding: EdgeInsets.all(16.0),
                      children: <Widget>[
                        Text(appData.labels['orden.cancel.confirm.label']),
                        SizedBox(
                          height: 16.0,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: SimpleDialogOption(
                                child: Text(appData.labels['admin.order.detail.cancel.confirm'],style: kConfirmDialogTextStyle),
                                onPressed: (){
                                  this.appData.newEmptyOrder();
                                  this.appData.notify();
                                  Navigator.pop(context);
                                  Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return NewOrderScreen();
                                      },
                                    ),
                                  );
                                },
                              ),
                            ),
                            Expanded(
                              child: SimpleDialogOption(
                                child: Text(appData.labels['admin.order.detail.cancel.no'],style: kCancelDialogTextStyle,),
                                onPressed: (){
                                  Navigator.pop(context);
                                },
                              ),
                            )
                          ],
                        ),

                      ],
                    );
                  });

            }
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            color: kSecondaryColor,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Text(
                    '${appData.labels['order.totalamount']} (${appData.order.order.length})',
                    style: kTotalAmountTextStyle,
                  )),
                  Text(
                    '${appData.order.totalAmount} ${appData.labels['settings.currency.cz']}',
                    style: kTotalAmountTextStyle,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: (appData.order.order.length > 0)
                ? ListOrder(
                    orders: appData.order.order,
                    updateAmount: updateTotalAmount(),
                  )
                : Center(
                    child: Text(
                      appData.labels['order.emptylist'],
                      style: kOrderEmptyTextStyle,
                    ),
                  ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    padding: EdgeInsets.all(16.0),
                    color: kAccentColor,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                        Text(
                          appData.labels['order.makenew'],
                          style: kButtonTextStyle,
                        )
                      ],
                    ),
                    onPressed: () {
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) {
                            return IngredientsScreen();
                          },
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                  padding: EdgeInsets.all(16.0),
                  color: kSecondaryColor,
                  disabledColor: Colors.red.shade200,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.shopping_cart,
                        color: Colors.white,
                      ),
                      Text(
                        appData.labels['order.pay'],
                        style: kButtonTextStyle,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Container(
                          height: 18.0,
                          width: 18.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white
                          ),
                          alignment: Alignment.center,
                          child: Text(appData.order.order.length.toString(),style: kBadgeStyle,),
                        ),
                      ),

                    ],
                  ),
                  onPressed: (appData.order.order.isEmpty)
                      ? null
                      : () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return ConfirmCheckoutScreen();
                        },
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
