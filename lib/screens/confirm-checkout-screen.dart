import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/model/order-message.dart';
import 'package:fosilapp/screens/ingredients-screen.dart';
import 'package:fosilapp/screens/new-order-screen.dart';
import 'package:fosilapp/screens/select-address-screen.dart';
import 'package:fosilapp/services/order-service.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

AppData appData;

class ConfirmCheckoutScreen extends StatefulWidget {
  @override
  _ConfirmCheckoutScreenState createState() => _ConfirmCheckoutScreenState();
}

class _ConfirmCheckoutScreenState extends State<ConfirmCheckoutScreen> {
  @override
  Widget build(BuildContext context) {
    appData = Provider.of<AppData>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(appData.labels['order.checkout']),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            if (appData.receivedOrder) Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Card(
                    color: kStatusColor,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Text(appData.labels['confirm.order.success.received.title'], style: kNewOrderBtnTextStyle, textAlign: TextAlign.center,),
                          Text(appData.labels['confirm.order.success.received.msg'], style: kButtonTextStyle, textAlign: TextAlign.center,),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16.0,right: 16.0,top:16.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      textColor: (appData.customerPickUp)?Colors.black38:kAccentColor,
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.directions_bike),
                          Text(appData.labels['confirm.order.delivery.service']),
                        ],
                      ),
                      onPressed:(){
                        appData.toggleDelivery();
                      },
                    ),
                  ),
                  Expanded(
                    child: FlatButton(
                      textColor: (appData.customerPickUp)?kAccentColor:Colors.black38,
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.directions_walk),
                          Text(appData.labels['confirm.order.customer.pickup']),
                        ],
                      ),
                      onPressed:(){
                        appData.toggleDelivery();
                      },
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 4.0,
                child: Container(
                  padding: EdgeInsets.all(16.0 ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      (!appData.customerPickUp)?FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        color: kAccentColor,
                        padding: EdgeInsets.all(16.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(FontAwesomeIcons.searchLocation, color: Colors.white,),
                            SizedBox(
                                width: 8.0,
                            ),
                            Text(appData.labels['delivery.address.search'], style: kButtonTextStyle),
                          ],
                        ),
                        onPressed: (){
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) {
                                return SelectAddressScreen();
                              },
                            ),
                          );
                        },
                      ):Container(),
                      (!appData.customerPickUp)?SizedBox(
                        height: 8.0,
                      ):Container(),
                      (!appData.customerPickUp)?TextField(
                        decoration: InputDecoration(
                            hintText: appData.labels['delivery.address.label']
                        ),
                        minLines: 1,
                        maxLines: 3,
                        readOnly: true,
                        controller: TextEditingController.fromValue(new TextEditingValue(text:appData.order.address)),
                      ):Container(),
                      TextField(
                        decoration: InputDecoration(
                            hintText: appData.labels['confirm.order.email']
                        ),
                        onChanged: (value){
                          appData.order.cc = value;
                        },
                      ),
                      TextField(
                      decoration: InputDecoration(
                          hintText: appData.labels['confirm.order.phonenumber'],
                      ),
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                        ], // On
                        onChanged: (value){
                          appData.order.phoneNumber = value;
                        },
                    ),
                      Padding(
                        padding: const EdgeInsets.only(top:16.0),
                        child: Row(
                          children: <Widget>[
                            Icon(FontAwesomeIcons.infoCircle,color: kAccentColor,),
                            SizedBox(
                              width: 16.0,
                            ),
                            Expanded(child: Text(appData.labels['confirm.order.msg.tempdata']))
                          ],
                        ),
                      )
                     ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 4.0,
                child: Container(
                  padding: EdgeInsets.all(16.0 ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(appData.labels['confirm.order.bill'], style: kGroupIngredientTextStyle,),
                      Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(child: Text('${appData.labels['confirm.order.items']} ${appData.order.order.length} ${appData.labels['confirm.order.items.type']}',style: kOrderTitleTextStyle,)),
                            Text('${appData.order.totalAmount} ${appData.labels['settings.currency.cz']}',style: kOrderTitleTextStyle,)
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(child: Text('${appData.labels['confirm.order.delivery']} (${(appData.order.deliveryDistance/1000).toStringAsFixed(2)} Km) ',style: kOrderTitleTextStyle,)),
                            (!appData.customerPickUp)?Text('${appData.order.deliveryAmount} ${appData.labels['settings.currency.cz']}', style: kOrderTitleTextStyle,)
                                :Text('0.0 ${appData.labels['settings.currency.cz']}', style: kOrderTitleTextStyle,)
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(child: Text('${appData.labels['order.totalamount']} ',style: kTotalTextStyle,)),
                            (!appData.customerPickUp)?Text('${appData.order.totalAmount + appData.order.deliveryAmount} ${appData.labels['settings.currency.cz']}', style: kTotalTextStyle,)
                                :Text('${appData.order.totalAmount} ${appData.labels['settings.currency.cz']}', style: kTotalTextStyle,)
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:16.0),
                        child: Row(
                          children: <Widget>[
                            Icon(FontAwesomeIcons.infoCircle,color: kAccentColor,),
                            SizedBox(
                              width: 16.0,
                            ),
                            Expanded(child: Text(appData.labels['confirm.order.onlycash']))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      if (!appData.receivedOrder)(
                          RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                            padding: EdgeInsets.all(16.0),
                            color: kSecondaryColor,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.send,color: Colors.white,),
                                SizedBox(
                                  width: 8.0,
                                ),
                                Expanded(
                                  child: Text(appData.labels['confirm.order.agreensend'],style: kButtonTextStyle,
                                    textAlign: TextAlign.left,),
                                ),
                              ],
                            ),
                            onPressed: (){
                              try{
                                if(appData.customerPickUp){
                                  appData.order.customerPickUp = true;
                                  appData.order.deliveryDistance = 0;
                                  appData.order.deliveryAmount = 0;
                                  appData.order.address = appData.labels['store.address'];
                                }
                                if(appData.order.address == null || appData.order.address.isEmpty ||
                                    appData.order.cc == null || appData.order.cc.isEmpty ||
                                    appData.order.phoneNumber == null || appData.order.phoneNumber.isEmpty){
                                  showDialog(context: context,
                                      builder: (BuildContext context){
                                        return SimpleDialog(
                                          title: Row(
                                            children: <Widget>[
                                              Icon(Icons.info_outline),
                                              SizedBox(width: 10.0,),
                                              Text(appData.labels['confirm.order.msg.title'])
                                            ],
                                          ),
                                          contentPadding: EdgeInsets.all(24.0),
                                          children: <Widget>[
                                            Text(appData.labels['confirm.order.msg.emptydata'])
                                          ],
                                        );
                                      });
                                } else {

                                  OrderMessage message = new OrderMessage();
                                  message.html =OrderService().buildEmail(appData.order);
                                  appData.order.message = message;

                                  OrderService().saveOrder(appData.order);
                                  appData.receivedOrder = true;
                                  appData.notify();
                                  Future.delayed(Duration(seconds: 3), () {
                                    appData.newOrder();
                                    appData.notify();
                                    Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(
                                        builder: (context) {
                                          return NewOrderScreen();
                                        },
                                      ),
                                    );
                                  });
                                }
                              } catch(ex){
                                print(ex);
                              }
                            },
                          )
                      )
                    ],
                  ),
                )
              ),
            ),


          ],
        ),
      ),
    );
  }
}
