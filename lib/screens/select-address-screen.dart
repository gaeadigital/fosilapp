import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/model/distance.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:provider/provider.dart';

class SelectAddressScreen extends StatefulWidget {
  @override
  _SelectAddressScreenState createState() => _SelectAddressScreenState();
}

class _SelectAddressScreenState extends State<SelectAddressScreen> {

  AppData appData;

  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  String queryAddress;

  List<ListTile> addressTiles;
  double searchContainer = 95.0;

  static const LatLng _center = const LatLng(50.046914,14.4058744);
  static const LatLng _fosilHome = const LatLng(50.046914,14.4058744);
  LatLng _home;
  final Set<Marker> _marker ={};
  LatLng _lastMapPosition= _center;
  MapType _currenMapType = MapType.normal;

  _onMapCreated(GoogleMapController controller){
    _mapController = controller;
    _controller.complete(controller);
  }

  _onCameraMove(CameraPosition position) async {
    _lastMapPosition = position.target;
  }

  _checkOnSelect() async{
    int updateHeight=0;
    searchContainer=95.0;
    var addresses = await Geocoder.local.findAddressesFromCoordinates(Coordinates(_lastMapPosition.latitude, _lastMapPosition.longitude));
    showModalBottomSheet(context: context, builder: (context) {
      return AddressList(addresses: addresses,selectAddress: _selectAddressFromList,);
    });
  }

  Widget button(Function function, IconData icon){
    return FloatingActionButton(
      onPressed: function,
      materialTapTargetSize: MaterialTapTargetSize.padded,
      backgroundColor: Colors.blue,
      child: Icon(icon, size: 36.0,),
    );
  }


  _selectAddressFromList(Address addressSelected) async {
    appData.order.address = addressSelected.addressLine;
    num distance = await Geolocator().distanceBetween(_fosilHome.latitude, _fosilHome.longitude, addressSelected.coordinates.latitude, addressSelected.coordinates.longitude);

    Distance distanceAmount = _getDistanceTotal(distance);
    print(distanceAmount.amount);
    appData.order.deliveryAmount = distanceAmount.amount;
    appData.order.deliveryDistance = distance;
    appData.notify();
    Navigator.pop(context);
  }

  _getDistanceTotal(num distance2Test){
    num startDistance = 0;
    Distance selected = appData.distancesAmount.last;
    appData.distancesAmount.forEach((distance) {
      print('${distance2Test}  : ${distance.distance} = ${distance.amount}');
      if(distance2Test>=startDistance && distance2Test<= distance.distance) {
        selected = distance;
      }
      startDistance = distance.distance;
    });
    return selected;
  }

  _startMarker() {
    _marker.add(Marker(
      markerId: MarkerId(_lastMapPosition.toString()),
      position: _lastMapPosition,
      infoWindow: InfoWindow(
          title: 'Fosil',
          snippet: 'Mexická restaurace'
      ),
      icon: BitmapDescriptor.defaultMarker,
    ));
  }

    _getLocation() async {
     List<ListTile> addressTilesLocal = [];
     if(queryAddress != null) {
       var addresses = await Geocoder.local.findAddressesFromQuery(
           queryAddress);

       showModalBottomSheet(context: context, builder: (context) {
         return AddressList(
           addresses: addresses, selectAddress: _selectAddressFromList,);
       });
     } else {
      showDialog(context: context,
      builder: (BuildContext context){
        return SimpleDialog(
          title: Row(
            children: <Widget>[
              Icon(Icons.info_outline),
              SizedBox(width: 10.0,),
              Text('Info')
            ],
          ),
          contentPadding: EdgeInsets.all(24.0),
          children: <Widget>[
            Text(appData.labels['delivery.instructions'])
          ],
        );
      });
     }
    }


  @override
  Widget build(BuildContext context) {
    _startMarker();
    appData = Provider.of<AppData>(context);
    addressTiles = [];
    return Scaffold(
      appBar: AppBar(
        title: Text(appData.labels['delivery.title']),
      ),
      body:
        Stack(
          children: <Widget>[
            GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 17.0
              ),
              mapType: _currenMapType,
              markers: _marker,
              onCameraMove: _onCameraMove,
            ),
            Align(
              alignment: Alignment.center,
              child: Icon(Icons.location_on, color: kAccentColor,),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topCenter,
                  child: Container(

                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      color: Colors.white,
                      boxShadow: [
                    BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 3,
                    offset: Offset(0, 3), )// changes position of shadow
                    ]
                  ),
                  padding: EdgeInsets.all(16.0),
                    height: searchContainer,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: TextField(
                    minLines:1,
                                maxLines: 2,
                                decoration: InputDecoration(
                                    hintText: appData.labels['delivery.instructions']
                                ),
                                onChanged: (value){
                                  queryAddress = value;
                                },
                              ),
                            ),
                            FlatButton(
                              color: kAccentColor,
                              padding: EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.search,color: Colors.white,size: 14.0,),
                                  Text(appData.labels['delivery.address.search.button'],style: kButtonSmallTextStyle,)
                                ],
                              ),
                              onPressed: _getLocation,
                            )
                          ],
                        ),
                        Expanded(
                          child: ListView(
                            children: addressTiles,
                          ),
                        )
                      ],
                    ),
                  ),
              ),
            ),
            Padding(padding: EdgeInsets.all(16.0),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Column(
                    mainAxisAlignment:  MainAxisAlignment.end,
                    children: <Widget>[
                      FloatingActionButton(
                        child: Icon(FontAwesomeIcons.searchLocation, color: Colors.white,),
                        onPressed: _checkOnSelect,
                      ),

                    ],
                  ),
                )
            ),

          ],
        )
    );
  }
}

class AddressList extends StatelessWidget {
  final List<Address> addresses;
  final Function selectAddress;

  AddressList({this.addresses, this.selectAddress});

  _buildList(context){
    AppData appData = Provider.of<AppData>(context);

    List<ListTile> tiles = [];
    var title =  ListTile(
      title: Text(appData.labels['delivery.address.list.selectone'],style: kInstructionsTextStyle,),
    );
    tiles.add(title);
    addresses.forEach((element) {
      print(element.addressLine);
      var tile = ListTile(
        onTap: (){
          selectAddress(element);
          Navigator.pop(context);
        },
        title: Text(element.addressLine),
        trailing: Icon(Icons.location_on),
      );
      tiles.add(tile);
    });
    return tiles;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: _buildList(context),
      ),
    );
  }
}
