import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/model/settings.dart';
import 'package:fosilapp/screens/ingredients-screen.dart';
import 'package:fosilapp/services/setting-services.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

AppData appData;

class NewOrderScreen extends StatefulWidget {
  @override
  _NewOrderScreenState createState() => _NewOrderScreenState();
}

class _NewOrderScreenState extends State<NewOrderScreen> {

  String backgroundImage = 'images/Burrito.jpg';
  bool containsBg = false;

  @override
  void initState() {
    super.initState();
  }

  getMainBackground(){
    String pathBG='';
    appData.settings.forEach((setting) {
      print(setting.uid);
      if(setting.uid == 'mainpage.background'){
        containsBg = true;
        print(setting.path);
        pathBG = setting.path;
      }
    });
    return pathBG;
  }

  @override
  Widget build(BuildContext context) {

    appData = Provider.of<AppData>(context);
    getMainBackground();
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: (containsBg)?NetworkImage(getMainBackground()):AssetImage(backgroundImage),
          fit:BoxFit.cover
        )
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left:16.0,right: 16.0),
            child: Container(
              height: 220.0,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: new BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
                image: DecorationImage(
                  image: AssetImage('images/Logo2.jpg'),
                  alignment: Alignment.bottomCenter
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(),
          ),

          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          side: BorderSide(color: kPrimaryColor)),
                      color: kPrimaryColor,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical:16.0,horizontal: 4.0),
                        child: Text('Vytvořit burrito',style: kButtonTextStyle,textAlign: TextAlign.center,),
                      ),
                      onPressed: () async{
                        appData.newOrder();
                        await appData.updateLabelsByLang('cs-CZ');
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) {

                              return IngredientsScreen();
                            },
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          side: BorderSide(color: kPrimaryColor)),
                      color: kPrimaryColor,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical:16.0,horizontal: 4.0),
                        child: Text('Make my burrito',style:kButtonTextStyle,textAlign: TextAlign.center,),
                      ),
                      onPressed: () async {
                        appData.newOrder();
                        await appData.updateLabelsByLang('en-US');
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) {

                              return IngredientsScreen();
                            },
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

