import 'package:fosilapp/model/content.dart';
import 'package:fosilapp/model/order-message.dart';

class Order {

  String address;
  num deliveryAmount;
  num deliveryDistance;
  String phoneNumber;
  String status;
  DateTime timestampRecord;
  num totalAmount;
  bool customerPickUp;
  String to = 'fosil@fosil.cz';
  String cc;
  OrderMessage message;

  List<Content> order;

  Order(){
    address='';
    deliveryAmount = 35.0;
    deliveryDistance = 0;
    phoneNumber = '';
    status='NEW';
    timestampRecord = DateTime.now();
    totalAmount = 0.0;

    order = List<Content>();

  }

  iterateContent(){
    List<Map<String, dynamic>> contents = [];
    order.forEach((order) => {
      contents.add(order.toJson())
    });
    return contents;
  }
}