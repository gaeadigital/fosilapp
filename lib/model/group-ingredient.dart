import 'package:fosilapp/model/ingredient.dart';

class GroupIngredient {

  String id;
  String detail;
  String label;
  int order;
  bool visible;

  List<Ingredient> ingredients;

  GroupIngredient({this.id, this.detail, this.label, this.order, this.visible});

}