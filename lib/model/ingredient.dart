class Ingredient {

  num amount;
  bool check;
  String group;
  String id;
  String label;
  int order;
  bool visible;

  Ingredient({this.id, this.label, this.group, this.amount, this.order, this.check, this.visible});

  Map<String, dynamic> toJson() => {
    'amount': amount,
    'check': check,
    'group': group,
    'id': id,
    'label': label,
    'order': order,
    'visible': visible
  };
}