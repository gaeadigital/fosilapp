
import 'package:fosilapp/model/ingredient.dart';

class Content {

  List<Ingredient> contents;
  String status;
  num total;

  Content(){
    status = 'NEW';
    total = 0.0;
    contents = List<Ingredient>();
  }

  List<Map<String, dynamic>> iterate() {
    List<Map<String, dynamic>> listMaps = [];
    contents.forEach((element) {
      listMaps.add(element.toJson());
    });
    return listMaps;
  }

  Map<String, dynamic> toJson() => {
    'status':status,
    'total':total,
    'contents':iterate()
  };

}