class OrderMessage {
  String hint = 'Nová objednávka';
  String subject = 'New order | Nová objednávka';
  String html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>New Order</title></head><body><h2>Nová objednávka v aplikaci</h2></body></html>';
}