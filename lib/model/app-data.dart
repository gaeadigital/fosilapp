import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fosilapp/model/distance.dart';
import 'package:fosilapp/model/group-ingredient.dart';
import 'package:fosilapp/model/order.dart';
import 'package:fosilapp/model/settings.dart';
import 'package:fosilapp/services/ingrediente-service.dart';
import 'package:fosilapp/services/setting-services.dart';
import 'package:package_info/package_info.dart';

final _firestore = Firestore.instance;

class AppData extends ChangeNotifier {

  String lang = 'en-US';
  Map<String, String> labels = new HashMap<String, String>();
  List<GroupIngredient> groups;
  List<Distance> distancesAmount;
  List<Settings> settings;
  Order order;
  bool customerPickUp = false;
  bool receivedOrder = false;
  PackageInfo packageInfo;

  getLabelsByLang(String lang) async{

   return await _firestore.collection('labels').getDocuments().then((querySnapshot) {
     querySnapshot.documents.forEach((label) {
       labels[label.documentID] = label.data[lang];
     });
   });
  }

  updateLabelsByLang(String lang) async {
    labels = new HashMap<String, String>();
    await getLabelsByLang(lang);
    notifyListeners();
  }

  getGroupIngredients() async{
    groups = await IngredientServices.loadGroupIngredients();
    for(var group in groups){
      group.ingredients = await IngredientServices.loadIngredientsByGroup(group.id);
    }
    notifyListeners();
  }

  getDistancesAmount() async{
    distancesAmount = await SettingServices.getDistances();
  }

  getSettings() async{
    settings = await SettingServices.getSettings();
  }

  newEmptyOrder() async {
    this.order = Order();
    this.receivedOrder = false;
  }

  newOrder(){
    this.order = Order();
    this.customerPickUp = false;
    this.receivedOrder = false;
    notifyListeners();
  }

  getPackageInfo() async{
      packageInfo = await PackageInfo.fromPlatform();
  }

  toggleDelivery(){
    if(this.customerPickUp){
      this.customerPickUp = false;
    } else {
      this.customerPickUp = true;
    }
    this.notify();
  }

  notify(){
    notifyListeners();
  }

  removeBurrito(){
    notifyListeners();
  }

}