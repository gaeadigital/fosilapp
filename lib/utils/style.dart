import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFFF0103);
const kSecondaryColor = Color(0xFFE90560);
const kAccentColor = Color(0xFFFF8A00);

const kStatusColor = Color(0xFF70DD5E);

const kTintColor = Color(0xFF780131);

const kNewOrderBtnTextStyle = TextStyle(
  fontSize: 28.0,
  color: Colors.white
);

const kButtonTextStyle = TextStyle(
    fontSize: 18.0,
    color: Colors.white
);

const kButtonSmallTextStyle = TextStyle(
    fontSize: 10.0,
    color: Colors.white
);

const kGroupIngredientTextStyle = TextStyle(
  fontSize: 24.0,
  color:kTintColor
);

const kTitleTextStyle = TextStyle(
    fontSize: 18.0,
    fontWeight: FontWeight.bold,
    color: Colors.white,
);

const kTotalAmountTextStyle = TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.bold,
    color: Colors.white,
);

const kInstructionsTextStyle = TextStyle(
    fontSize: 18.0,
    color: kAccentColor,
);

const kOrderTitleTextStyle = TextStyle(
    fontSize: 16.0,
    color: Colors.black87,
    fontWeight: FontWeight.bold
);

const kOrderIngredientsTextStyle = TextStyle(
    fontSize: 14.0,
    color: Colors.black54
);

const kOrderEmptyTextStyle = TextStyle(
    fontSize: 24.0,
    color: Colors.black26,
    fontWeight: FontWeight.bold
);

const kTotalTextStyle = TextStyle(
    fontSize: 22.0,
    fontWeight: FontWeight.bold,
    color: kTintColor,
);

const kConfirmDialogTextStyle = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    color: kAccentColor
);

const kCancelDialogTextStyle = TextStyle(
    fontSize: 14.0,
    color: Colors.black54
);

const kBadgeStyle = TextStyle(
    fontSize: 12.0,
    color: kSecondaryColor,
    fontWeight: FontWeight.bold
);