import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fosilapp/model/group-ingredient.dart';
import 'package:fosilapp/model/ingredient.dart';

final _firestore = Firestore.instance;

class IngredientServices {

  static String groupIngredientCollectionName = 'group-ingredient';
  static String ingredientCollectionName = 'ingredients';

  get() {
    return  _firestore.collection(groupIngredientCollectionName).orderBy('order',descending: false).snapshots();
  }

  getByGroup(String groupId) {
    return  _firestore.collection(ingredientCollectionName).where('group',isEqualTo: groupId).orderBy('order',descending: false).snapshots();
  }

  static loadGroupIngredients() async{
    final List<DocumentSnapshot> documents  = (await _firestore.collection(groupIngredientCollectionName).orderBy('order',descending: false).getDocuments()).documents;
    return documents.map((groupDoc) => GroupIngredient(id: groupDoc.documentID,detail: groupDoc.data['detail'],label: groupDoc.data['label'],order: groupDoc.data['order'])).toList();
  }

   static loadIngredientsByGroup(String groupId) async{
    List<Ingredient> ingredients = List<Ingredient>();
     final List<DocumentSnapshot> documents  = (await _firestore.collection(ingredientCollectionName).where('group',isEqualTo: groupId).where('visible',isEqualTo: true).orderBy('order',descending: false).getDocuments()).documents;

     ingredients = documents.map((docSh) => Ingredient(id:docSh.documentID,
         label: docSh.data['label'],
         group:docSh.data['group'],
         amount: docSh.data['amount'],
         order: docSh.data['order'],
         check: (docSh.data['check']==true)?true:false,
         visible: (docSh.data['visible']==true)?true:false,
     )).toList();

     return ingredients;
  }
}