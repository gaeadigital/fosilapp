import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fosilapp/model/distance.dart';
import 'package:fosilapp/model/settings.dart';

final _firestore = Firestore.instance;

class SettingServices{

  static String languageCollectionName = 'language';
  static String distancesCollectionName = 'distances';
  static String settingsCollectionName = 'settings';

  getAvailableLanguage() {
    return  _firestore.collection(languageCollectionName).snapshots();
  }

  static getDistances() async{
    final List<DocumentSnapshot> documents  = (await _firestore.collection(distancesCollectionName).orderBy('amount',descending: false).getDocuments()).documents;
    return documents.map((distanceDoc) => Distance(distance: num.parse(distanceDoc.documentID),label: distanceDoc.data['label'],amount: distanceDoc.data['amount'])).toList();

  }

  /**
   * Obtener las configuraciones generales.
   */
  static getSettings() async {
    final List<DocumentSnapshot> documents  = (await _firestore.collection(settingsCollectionName).getDocuments()).documents;
    return documents.map((distanceDoc) => Settings(uid:distanceDoc.documentID,label: distanceDoc.data['label'],path: distanceDoc.data['path'])).toList();
  }

}