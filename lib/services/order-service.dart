import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:fosilapp/component/list-order.dart';
import 'package:fosilapp/model/order.dart';

final _firestore = Firestore.instance;

class OrderService {

  static String orderCollectionName = 'orders';

  saveOrder(Order order) async {
    try{

      String id = _firestore.collection(orderCollectionName).document().documentID;
      print('ORDER ID :${id}');
      Map<String, dynamic> data = {
        'id':id,
        'address': order.address,
        'deliveryAmount': order.deliveryAmount,
        'phoneNumber': order.phoneNumber,
        'status': order.status,
        'order':order.iterateContent(),
        'timestampRecord': new DateTime.now(),
        'totalAmount': order.totalAmount,
        'to':order.to,
        'cc':order.cc,
        'customerPickUp':order.customerPickUp,
        'message':{
          'hint':order.message.hint,
          'subject':order.message.subject,
          'html':order.message.html,
        }
      };

      return await _firestore.collection(orderCollectionName).document(id).setData(data,merge: true);
    } catch(e){
      print(e);
      throw(e);
    }
  }

  buildContentOrder(Order order){
    String content = '';

    order.order.forEach((element) {
      String detail='';
      element.contents.forEach((ingredient) {
        detail+= '<span style="font-size:10px; color:#666;">${appData.labels[ingredient.label]}, </span>';
      });
      content+='<tr><td><h5 style="margin:0px;">Burrita</h5>${detail}</td>'
      +'<td style="text-align: right;">Kč  ${element.total}</td></tr>';
    });

    return content;
  }

  buildEmail(Order order){
    return '<div style="max-width: 450px;margin: auto !important;padding: 1em; border:solid 1px #ddd;border-radius:10px;">'
    +'<table style="width:100%"><thead><tr><th colspan="2" style="text-align: right;">'
    +'<img style="width:100px"  src="https://firebasestorage.googleapis.com/v0/b/fosil-pwa.appspot.com/o/settings%2Flogos%2FLogo2.jpg?alt=media&token=b28964ab-e5d8-4f51-a783-d4d4815b4dad">'
    +'</th></tr><tr><th style="text-align: left;"><h3>New Order | Nová objednávka</h3></th><th style="text-align: right;">'
    +'<label style="font-size:10px;color:#666;">Received | Obdržel</label><p style="margin:0;">${formatDate(order.timestampRecord, [dd, '. ', mm, '. ', yyyy, ' ',HH,':',nn])}</p></th></tr></thead>'
    +'<tbody><tr><td colspan="2" style="padding-bottom:2rem">'
    +'<label style="font-size:10px;color:#666;">Address | Adresa</label><p style="margin:0;">${order.address}</p>'
    +'<label style="font-size:10px;color:#666;">Phone number | Telefonní číslo</label><p style="margin:0;">${order.phoneNumber}</p></td></tr>'
        +'<tr><td colspan="2" ><hr></td></tr>'
    +buildContentOrder(order)
        +'<tr><td colspan="2" ><hr></td></tr>'
    +'<tr><td colspan="2" ><h4>Bill | Účtovat</h4></td></tr>'
    +'<tr><td>Order | Objednat: ${order.order.length} burrito(s)</td> <td style="text-align: right;">Kč ${order.totalAmount}</td></tr>'
    +'<tr><td>Delivery | Dodávka: ( ${order.deliveryDistance} km)</td> <td style="text-align: right;">Kč ${order.deliveryAmount}</td></tr>'
    +'<tr><td colspan="2"> <hr></td></tr>'
    +'</tbody> '
    +'<tfoot><tr><td><h4>Total | Celkový<h4></td>'
    +'<td style="text-align: right;"> <h4>Kč ${order.totalAmount + order.deliveryAmount}</h4></td></tr></tfoot></table><div>';
  }
}