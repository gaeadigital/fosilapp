import 'package:flutter/material.dart';
import 'package:fosilapp/model/app-data.dart';
import 'package:fosilapp/screens/loading-screen.dart';
import 'package:fosilapp/utils/style.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create:(_)=>AppData(),
      child: MaterialApp(
        title: 'Fosil App',
        theme: ThemeData(
          primaryColor: kPrimaryColor,
          accentColor: kAccentColor,
          primarySwatch: Colors.red,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: LoadingScreen(),
      ),
    );
  }
}
